<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
  //  return view('welcome');
//});


Route::get('/', 'wellcomecontroller@index');
Route::get('/catagories', 'wellcomecontroller@catagories');
Route::get('/womenscatagories', 'wellcomecontroller@womenscatagories');
Route::get('/quickview', 'wellcomecontroller@quickview');
Route::get('/electronics', 'wellcomecontroller@electronics');
Route::get('/codes', 'wellcomecontroller@codes');
Route::get('/contact', 'wellcomecontroller@contact');
Auth::routes();

Route::get('/dashboard', 'HomeController@index')->name('dashboard');
Route::get('/Catagory/add', 'CatagoryController@CreateCatagory')->name('CreateCatagory');
Route::post('/catagoty/save', 'CatagoryController@storeCatagory')->name('storeCatagory');
Route::get('/ManageCatagories', 'CatagoryController@ManageCatagories')->name('ManageCatagories');