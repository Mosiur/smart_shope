@extends('admin.master')
@section('content')
      <div class="row" style="margin-top: 0px;padding: 50px;">
        <div class="col-lg-12">
          <div class="well">
          	{!!Form::open(['url'=>'/catagoty/save','method'=>'POST','class'=>'form-horizontal'])!!}
          	<div class="form-group">
                <label class="col-sm-2 control-lebel">Catagory Name</label>
                <div class="col-sm-10">
                  <input type="text" name="CatagoryName" required class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-lebel">Catagory Description</label>
                <div class="col-sm-10">
                  <textarea class="form-control" name="Catagorydescription"></textarea> 
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-lebel">Publication Status</label>
                <div class="col-sm-10">
                  <select class="form-control" name="Publicationstatus">
                    <option>select publication status</option>
                    <option>Published</option>
                    <option>Unpublish</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" name="btn" class="btn btn-success btn-block">Save catagore</button>
                  </div>
              </div>
            {!!Form::close()!!}
          </div>
        </div>
      </div> 
@endsection