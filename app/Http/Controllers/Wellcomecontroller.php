<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Wellcomecontroller extends Controller
{
    public function index(){

    	return view('frontEnd.home.homeContent');
    }

    public function catagories(){

    	return view('frontEnd.catagories.mens');
    }
     public function womenscatagories(){

    	return view('frontEnd.catagories.womens');
    }
    public function quickview(){

    	return view('frontEnd.include.single');
    }
    public function electronics(){

    	return view('frontEnd.catagories.electronics');
    }
    public function codes(){

    	return view('frontEnd.catagories.codes');
    }
    public function contact(){

    	return view('frontEnd.catagories.contact');
    }
}
