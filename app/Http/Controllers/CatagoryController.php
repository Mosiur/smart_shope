<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Catagory;
use DB;

class CatagoryController extends Controller
{
   public function CreateCatagory()
    {
        return view('admin.Catagory.CreateCatagory');
    }

    public function storeCatagory(Request $request)
    {
    	//Eliquent...............

        //return $request->all();
    	//$catagory = new Catagory();
    	//$catagory ->CatagoryName = $request->CatagoryName;
    	//$catagory ->Catagorydescription = $request->Catagorydescription;
    	//$catagory ->Publicationstatus = $request->Publicationstatus;
    	//$catagory ->save();
        //Catagory::create($request->all());


        //Query builder..........


        DB::table('catagories')->insert([
        'CatagoryName' => $request->CatagoryName,
        'Catagorydescription' => $request->Catagorydescription,
        'Publicationstatus' => $request->Publicationstatus,
        ]);

    	return 'successfully saved';
    }

    public function ManageCatagories()
    {
        return view('admin.Catagory.ManageCatagories');
    }
}
